////  SettingsScreen.swift
//  SwiftUI2_SwiftUIWithUIKit
//
//  Created on 09/02/2021.
//  
//

import SwiftUI

struct SettingsScreen: View {
    
    @State private var isOn = false
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Toggle("Is Subsktibe", isOn: $isOn)
                    Toggle("Is Subsktibe", isOn: $isOn)
                    Toggle("Is Subsktibe", isOn: $isOn)
                    Spacer()
                }
            }
            .navigationTitle("Setting")
        }
    }
}

struct SettingsScreen_Previews: PreviewProvider {
    static var previews: some View {
        SettingsScreen()
    }
}
